from django.urls import path
from apps.integration.pokemones.views import BasicDataPokemon


urlpatterns = [
    path(
        'pokemones/basicdata/<str:name>',
        BasicDataPokemon.as_view(),
        name='basicdata'
    )
]
