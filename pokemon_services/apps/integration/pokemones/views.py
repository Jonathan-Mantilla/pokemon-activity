from rest_framework import response, serializers, views, status
from apps.integration.pokemones import services as pokemones_services


class BasicDataPokemon(views.APIView):

    class OutputSerializer(serializers.Serializer):
        id = serializers.IntegerField()
        name = serializers.CharField()
        evolutions = serializers.CharField()
        powers = serializers.CharField()

    def get(self, request, name):

        output_data = pokemones_services.get_data_pokemon(
            name_pokemon=name
        )

        try:
            response_data = self.OutputSerializer(output_data).data

        except Exception:
            msg = 'BasicDataPokemon :: an error occurred'
            raise msg

        return response.Response(data=response_data, status=status.HTTP_200_OK)
