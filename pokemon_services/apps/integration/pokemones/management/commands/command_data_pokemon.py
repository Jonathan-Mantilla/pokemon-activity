from django.core.management.base import BaseCommand, CommandError
from apps.integration.pokemones import services
from apps.integration.pokemones.models import PokemonInfo, PokemonEvolution


class Command(BaseCommand):
    help = ('For execute command, follow example --> '
            'python pokemon_services/manage.py '
            'command_data_pokemon --id=1')

    def add_arguments(self, parser):

        parser.add_argument(
            '--id',
            dest='Id_pokemon',
            required=True,
            nargs='?',
            type=int
        )
        
    def handle(self, *args, **options):
        
        id_pokemon = options.get("Id_pokemon")
        
        if not id_pokemon:
            raise CommandError(
                'Failed arguments...example -> '
                '--id_pokemon=1'
            )
        
        try:
            
            data = services.data_pokemon(id_pokemon=id_pokemon)
            evolutions = data['evolutions']
            
            pokemon = PokemonInfo.objects.create(
                id_pokemon=data['id'],
                name=data['name'],
                height=data['height'],
                weight=data['weight'],
                evolutions=evolutions,
                powers=data['powers']
            )
            for evolution in evolutions:
                PokemonEvolution.objects.create(
                    name=evolution['name'],
                    min_level=evolution['min_level'],
                    overworld_rain=evolution['needs_overworld_rain'],
                    upside_down=evolution['turn_upside_down'],
                    pokemon=pokemon
                )

            print(f'Created Pokemon Information {id_pokemon}')
        except Exception:
            msg = f'Add argument {id_pokemon}'
            raise msg