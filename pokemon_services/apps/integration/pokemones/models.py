from django.utils.translation import ugettext_lazy as _
from django.db import models


class BaseModel(models.Model):

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_(u'created at')
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        verbose_name=_(u'updated at')
    )

    class Meta:
        abstract = True


class PokemonInfo(BaseModel):

    id_pokemon = models.IntegerField(
        verbose_name=_('id pokemon')
    )
    name = models.CharField(
        null=True,
        max_length=30,
        verbose_name=_('name')
    )
    height = models.CharField(
        null=True,
        max_length=20,
        verbose_name=_('height')
    )
    weight = models.CharField(
        null=True,
        max_length=20,
        verbose_name=_('weight')
    )
    evolutions = models.TextField(
        null=True,
        verbose_name=_('evolutions')
    )
    powers = models.TextField(
        verbose_name=_('powers')
    )


class PokemonEvolution(BaseModel):

    name = models.CharField(
        null=True,
        max_length=30,
        verbose_name=_('name_evolution')
    )
    min_level = models.IntegerField(
        verbose_name=_('min_level')
    )
    overworld_rain = models.BooleanField(
        verbose_name=_('overworld_rain')
    )
    upside_down = models.BooleanField(
        verbose_name=_('upside_down')
    )
    pokemon = models.ForeignKey(
        PokemonInfo,
        null=True,
        on_delete=models.DO_NOTHING,
        related_name='pokemon',
        verbose_name=_('pokemon')
    )
