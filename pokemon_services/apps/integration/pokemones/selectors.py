from django.db.models import QuerySet

from apps.integration.pokemones.models import PokemonInfo, PokemonEvolution


def get_pokemon_by_name(
    *,
    name_pokemon: str
) -> 'QuerySet[PokemonInfo]':

    return PokemonInfo.objects.filter(
        name=name_pokemon
    )
