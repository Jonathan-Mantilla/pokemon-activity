from typing import Dict, List, Optional, Any
import pokebase as pokemon_db
from pokebase import interface

from apps.integration.pokemones import selectors as pokemon_selectors


def data_pokemon(
    *,
    id_pokemon: int

) -> Dict:
    pokemon = pokemon_db.pokemon(id_or_name=id_pokemon)
    try:
        pokemon_id = pokemon.id
    except Exception:
        msg = 'There is no pokemon'
        raise msg

    pokemon_name = pokemon.name
    pokemon_power = pokemon_db.stat(id_or_name=pokemon_name)
    power = data_powers_pokemon(pokemon_power=pokemon_power.results)
    pokemon_evolution = pokemon_db.evolution_chain(id_=int(pokemon_id))
    evolutions = filter_evolutions_pokemon(evolution=pokemon_evolution.chain)

    pokemon_height = pokemon.height
    pokemon_name = pokemon.name
    pokemon_weight = pokemon.weight

    data = dict(
        id=pokemon_id,
        name=pokemon_name,
        height=pokemon_height,
        weight=pokemon_weight,
        evolutions=evolutions,
        powers=power
    )

    return data


def filter_evolutions_pokemon(
    *,
    evolution: interface
) -> List[Dict]:
    pokemon_evolutions = evolution.evolves_to[0]
    data = []

    evolutions_pokemon = True
    while evolutions_pokemon:

        info_pokemones = data_pokemones(evolution=pokemon_evolutions)
        data.append(info_pokemones)

        try:

            pokemon_evolutions = pokemon_evolutions.evolves_to[0]

        except IndexError:
            evolutions_pokemon = False

    return data


def data_pokemones(
    *,
    evolution
) -> Dict:
    evolution_pokemon_details = evolution.evolution_details[0]
    evolution_pokemon_specie = evolution.species.name

    data = dict(
        name=evolution_pokemon_specie,
        min_level=evolution_pokemon_details.min_level,
        needs_overworld_rain=evolution_pokemon_details.needs_overworld_rain,
        turn_upside_down=evolution_pokemon_details.turn_upside_down
    )

    return data


def data_powers_pokemon(
    *,
    pokemon_power: List
) -> List:
    power_pokemon = []
    for power in pokemon_power:
        type_power = power.name
        power_pokemon.append(type_power)

    return power_pokemon


def get_data_pokemon(
    *,
    name_pokemon: str
) -> Dict:
    pokemon = pokemon_selectors.get_pokemon_by_name(
        name_pokemon=name_pokemon
    ).first()

    if not pokemon:
        msg = 'There is no a pokemon in the db'
        raise msg

    data = dict(
        id=pokemon.id_pokemon,
        name=pokemon.name,
        evolutions=pokemon.evolutions,
        powers=pokemon.powers
    )

    return data
